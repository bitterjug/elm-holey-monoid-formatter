module Formatter exposing (..)

import HoleyMonoid exposing (HoleyMonoid, append, later, now, run)


type alias Formatter r a_r =
    HoleyMonoid String r a_r


s : String -> Formatter r a_r -> Formatter r a_r
s =
    append << now


fmt : (p -> String) -> Formatter (p -> r) a_p_r -> Formatter r a_p_r
fmt f =
    append (later f)


string : Formatter (String -> r) a_string_r -> Formatter r a_string_r
string =
    append (later identity)


int : Formatter (Int -> r) a_int_r -> Formatter r a_int_r
int =
    fmt String.fromInt


float : Formatter (Float -> r) a_int_r -> Formatter r a_int_r
float =
    fmt String.fromFloat


format : (Formatter r r -> (a -> a) -> b) -> b
format =
    run ""
