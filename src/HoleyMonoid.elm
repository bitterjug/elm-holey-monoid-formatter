module HoleyMonoid exposing (..)


type alias HoleyMonoid appendable r a_r =
    -- a_r is that type that is constrained by construction to
    -- be either r or a function of some argument to r (a -> r).
    -- Remember that a itself might be a funtion.
    -- We call the (appendable -> r) a transformer tx,
    -- frequently r is eventually bound to the appendable type
    (appendable -> r) -> a_r



-- Constructors


now : appendable -> HoleyMonoid appendable r r
now m =
    \tx -> tx m


later : (a -> appendable) -> HoleyMonoid appendable r (a -> r)
later f =
    \tx -> f >> tx



-- indexed bind prserving the relationship between r and a_r


andThen : (appendable -> HoleyMonoid appendable r b_r) -> HoleyMonoid appendable b_r a_b_r -> HoleyMonoid appendable r a_b_r
andThen m2second first =
    \tx ->
        first (\m -> m2second m tx)



-- append : HoleyMonoid appendable a b -> HoleyMonoid appendable b c -> HoleyMonoid appendable a c


append : HoleyMonoid appendable r b_r -> HoleyMonoid appendable b_r a_b_r -> HoleyMonoid appendable r a_b_r
append second first =
    first
        |> andThen
            (\m1 ->
                second
                    |> andThen (\m2 -> now (m1 ++ m2))
            )


run : appendable -> (HoleyMonoid appendable r r -> (a -> a) -> b) -> b
run unit fmt =
    (now unit |> fmt) identity
