module Template exposing (..)

import Html exposing (Html)


-- (later .h identity: F r1 ({x|h : String}->r1)
--   |> append (later .w String.fromInt : F r2 ({y|w : Int}->r2))
--                        ----> F r2 ({z| h: String, w: Int}->r2)
--
-- (lhs: F r1 (c->r1) |> append (rhs : F r2 (c->r2)) ~~~> F r2 (c->r2)
-- (lhs: F r1 (r1)    |> append (rhs : F r2 (c->r2)) ~~~> F r2 (c->r2)
-- (lhs: F r1 (c->r1) |> append (rhs : F r2 (r2))    ~~~> F r2 (c->r2)


type T appendable context
    = T ((appendable -> appendable) -> (context -> appendable))


now : appendable -> T appendable context
now m =
    T <|
        \tx _ -> tx m


later : (context -> a) -> (a -> appendable) -> T appendable context
later selector formatter =
    T <|
        \transform ->
            selector >> formatter >> transform


appendLater : (context -> a) -> (a -> appendable) -> T appendable context -> T appendable context
appendLater selector formatter =
    append (later selector formatter)


andThen : (appendable -> T appendable context) -> T appendable context -> T appendable context
andThen f (T process1) =
    T <|
        \tx context ->
            let
                transformer m =
                    let
                        (T process2) =
                            f m
                    in
                    process2 tx context
            in
            process1 transformer context



-- \tx c -> t (\m -> f m tx c) c


append : T appendable context -> T appendable context -> T appendable context
append rhs lhs =
    lhs
        |> andThen
            (\m1 ->
                rhs
                    |> andThen
                        (\m2 ->
                            now (m1 ++ m2)
                        )
            )


apply : context -> T appendable context -> appendable
apply context (T process) =
    process identity context



-- String template formatter


format : context -> (T String context -> T String context) -> String
format context templateTransformer =
    let
        unit =
            now ""

        (T process) =
            templateTransformer unit
    in
    process identity context


s : String -> T String context -> T String context
s str =
    append (now str)


string : (context -> String) -> T String context -> T String context
string selector =
    append (later selector identity)


int : (context -> Int) -> T String context -> T String context
int selector =
    append (later selector String.fromInt)


float : (context -> Float) -> T String context -> T String context
float selector =
    append (later selector String.fromFloat)


fmt : (context -> a) -> (a -> String) -> T String context -> T String context
fmt =
    appendLater



-- List of HTML formatter


type alias Template context msg =
    T (List (Html msg)) context


text : String -> Template context msg -> Template context msg
text str =
    append <| now [ Html.text str ]


string1 : (context -> String) -> Template context msg -> Template context msg
string1 selector =
    appendLater selector (Html.text >> List.singleton)


render : context -> (Template context msg -> Template context msg) -> List (Html msg)
render context templateTransformer =
    let
        unit =
            now []

        (T process) =
            templateTransformer unit
    in
    process identity context


element : (context -> a) -> (a -> Html msg) -> Template context msg -> Template context msg
element selector formatter =
    appendLater selector (formatter >> List.singleton)
