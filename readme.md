Holy Monoid Formatter
=====================

Experimental formatting library based on Holey Monoid after
https://www.stackage.org/haddock/nightly-2017-09-15/formatting-6.2.5/Formatting.html

# TODO

- [ ] Write some tests
- [ ] Write some docs
- [ ] Publish
